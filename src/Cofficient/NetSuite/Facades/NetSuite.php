<?php
/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 17/10/2015
 * Time: 08:23
 */

namespace Cofficient\NetSuite\Facades;

use Illuminate\Support\Facades\Facade;

class NetSuite extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'netsuite';
    }
}