<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SolutionTopics
 */
class SolutionTopics
{
    /**
     * @access public
     * @var RecordRef
     */
    public $topic;
    static $paramtypesmap = array('topic' => 'RecordRef');
}