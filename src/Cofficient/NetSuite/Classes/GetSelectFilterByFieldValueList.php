<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetSelectFilterByFieldValueList
 */
class GetSelectFilterByFieldValueList
{
    /**
     * @access public
     * @var GetSelectFilterByFieldValue[]
     */
    public $filterBy;
    static $paramtypesmap = array('filterBy' => 'GetSelectFilterByFieldValue[]');
}