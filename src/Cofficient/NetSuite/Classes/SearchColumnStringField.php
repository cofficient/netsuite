<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchColumnStringField
 */
class SearchColumnStringField extends SearchColumnField
{
    /**
     * @access public
     * @var string
     */
    public $searchValue;
    static $paramtypesmap = array('searchValue' => 'string');
}