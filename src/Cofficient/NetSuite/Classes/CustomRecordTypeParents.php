<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CustomRecordTypeParents
 */
class CustomRecordTypeParents
{
    /**
     * @access public
     * @var string
     */
    public $childDescr;
    static $paramtypesmap = array('childDescr' => 'string');
}