<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CustomerStatusSearch
 */
class CustomerStatusSearch extends SearchRecord
{
    /**
     * @access public
     * @var CustomerStatusSearchBasic
     */
    public $basic;
    /**
     * @access public
     * @var EmployeeSearchBasic
     */
    public $userJoin;
    static $paramtypesmap = array('basic' => 'CustomerStatusSearchBasic', 'userJoin' => 'EmployeeSearchBasic');
}