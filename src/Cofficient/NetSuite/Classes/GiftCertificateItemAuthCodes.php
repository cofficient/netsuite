<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GiftCertificateItemAuthCodes
 */
class GiftCertificateItemAuthCodes
{
    /**
     * @access public
     * @var string
     */
    public $authCode;
    /**
     * @access public
     * @var boolean
     */
    public $used;
    static $paramtypesmap = array('authCode' => 'string', 'used' => 'boolean');
}