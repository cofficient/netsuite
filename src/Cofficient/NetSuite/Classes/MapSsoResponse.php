<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * MapSsoResponse
 */
class MapSsoResponse
{
    /**
     * @access public
     * @var SessionResponse
     */
    public $sessionResponse;
    static $paramtypesmap = array('sessionResponse' => 'SessionResponse');
}