<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * PricingGroupSearch
 */
class PricingGroupSearch extends SearchRecord
{
    /**
     * @access public
     * @var PricingGroupSearchBasic
     */
    public $basic;
    /**
     * @access public
     * @var EmployeeSearchBasic
     */
    public $userJoin;
    static $paramtypesmap = array('basic' => 'PricingGroupSearchBasic', 'userJoin' => 'EmployeeSearchBasic');
}