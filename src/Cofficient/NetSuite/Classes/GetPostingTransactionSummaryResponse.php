<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetPostingTransactionSummaryResponse
 */
class GetPostingTransactionSummaryResponse
{
    /**
     * @access public
     * @var GetPostingTransactionSummaryResult
     */
    public $getPostingTransactionSummaryResult;
    static $paramtypesmap = array('getPostingTransactionSummaryResult' => 'GetPostingTransactionSummaryResult');
}