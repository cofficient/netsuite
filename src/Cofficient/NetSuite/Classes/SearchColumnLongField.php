<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchColumnLongField
 */
class SearchColumnLongField extends SearchColumnField
{
    /**
     * @access public
     * @var integer
     */
    public $searchValue;
    static $paramtypesmap = array('searchValue' => 'integer');
}