<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * LandedCost
 */
class LandedCost extends Record
{
    /**
     * @access public
     * @var LandedCostDataList
     */
    public $landedCostDataList;
    static $paramtypesmap = array('landedCostDataList' => 'LandedCostDataList');
}