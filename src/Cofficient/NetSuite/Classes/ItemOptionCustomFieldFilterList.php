<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ItemOptionCustomFieldFilterList
 */
class ItemOptionCustomFieldFilterList
{
    /**
     * @access public
     * @var ItemOptionCustomFieldFilter[]
     */
    public $filter;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('filter' => 'ItemOptionCustomFieldFilter[]', 'replaceAll' => 'boolean');
}