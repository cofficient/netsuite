<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * DeleteListRequest
 */
class DeleteListRequest
{
    /**
     * @access public
     * @var BaseRef[]
     */
    public $baseRef;
    static $paramtypesmap = array('baseRef' => 'BaseRef[]');
}