<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AsyncStatusResponse
 */
class AsyncStatusResponse
{
    /**
     * @access public
     * @var AsyncStatusResult
     */
    public $asyncStatusResult;
    static $paramtypesmap = array('asyncStatusResult' => 'AsyncStatusResult');
}