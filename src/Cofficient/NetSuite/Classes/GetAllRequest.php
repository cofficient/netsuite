<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetAllRequest
 */
class GetAllRequest
{
    /**
     * @access public
     * @var GetAllRecord
     */
    public $record;
    static $paramtypesmap = array('record' => 'GetAllRecord');
}