<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * InitializeListResponse
 */
class InitializeListResponse
{
    /**
     * @access public
     * @var ReadResponseList
     */
    public $readResponseList;
    static $paramtypesmap = array('readResponseList' => 'ReadResponseList');
}