<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ItemReceiptItemList
 */
class ItemReceiptItemList
{
    /**
     * @access public
     * @var ItemReceiptItem[]
     */
    public $item;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('item' => 'ItemReceiptItem[]', 'replaceAll' => 'boolean');
}