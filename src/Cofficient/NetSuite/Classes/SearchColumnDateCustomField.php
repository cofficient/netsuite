<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchColumnDateCustomField
 */
class SearchColumnDateCustomField extends SearchColumnCustomField
{
    /**
     * @access public
     * @var dateTime
     */
    public $searchValue;
    static $paramtypesmap = array('searchValue' => 'dateTime');
}