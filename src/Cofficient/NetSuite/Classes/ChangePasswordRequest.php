<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ChangePasswordRequest
 */
class ChangePasswordRequest
{
    /**
     * @access public
     * @var ChangePassword
     */
    public $changePassword;
    static $paramtypesmap = array('changePassword' => 'ChangePassword');
}