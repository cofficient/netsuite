<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * InitializeRequest
 */
class InitializeRequest
{
    /**
     * @access public
     * @var InitializeRecord
     */
    public $initializeRecord;
    static $paramtypesmap = array('initializeRecord' => 'InitializeRecord');
}