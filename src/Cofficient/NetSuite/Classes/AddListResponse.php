<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AddListResponse
 */
class AddListResponse
{
    /**
     * @access public
     * @var WriteResponseList
     */
    public $writeResponseList;
    static $paramtypesmap = array('writeResponseList' => 'WriteResponseList');
}