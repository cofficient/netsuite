<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * UpdateInviteeStatusListResponse
 */
class UpdateInviteeStatusListResponse
{
    /**
     * @access public
     * @var WriteResponseList
     */
    public $writeResponseList;
    static $paramtypesmap = array('writeResponseList' => 'WriteResponseList');
}