<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ExceededRequestLimitFault
 */
class ExceededRequestLimitFault extends NSSoapFault
{
    static $paramtypesmap = array();
}