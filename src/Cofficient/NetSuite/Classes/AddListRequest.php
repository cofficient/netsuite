<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AddListRequest
 */
class AddListRequest
{
    /**
     * @access public
     * @var Record[]
     */
    public $record;
    static $paramtypesmap = array('record' => 'Record[]');
}