<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AttachRequest
 */
class AttachRequest
{
    /**
     * @access public
     * @var AttachReference
     */
    public $attachReference;
    static $paramtypesmap = array('attachReference' => 'AttachReference');
}