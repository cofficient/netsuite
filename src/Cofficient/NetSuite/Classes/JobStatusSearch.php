<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * JobStatusSearch
 */
class JobStatusSearch extends SearchRecord
{
    /**
     * @access public
     * @var JobStatusSearchBasic
     */
    public $basic;
    /**
     * @access public
     * @var EmployeeSearchBasic
     */
    public $userJoin;
    static $paramtypesmap = array('basic' => 'JobStatusSearchBasic', 'userJoin' => 'EmployeeSearchBasic');
}