<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ItemInvtClassification
 */
class ItemInvtClassification
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _a = '_a';
    /**
     * @var string
     */
    const _b = '_b';
    /**
     * @var string
     */
    const _c = '_c';
}