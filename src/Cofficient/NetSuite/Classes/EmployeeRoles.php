<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * EmployeeRoles
 */
class EmployeeRoles
{
    /**
     * @access public
     * @var RecordRef
     */
    public $selectedRole;
    static $paramtypesmap = array('selectedRole' => 'RecordRef');
}