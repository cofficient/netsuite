<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetAllResponse
 */
class GetAllResponse
{
    /**
     * @access public
     * @var GetAllResult
     */
    public $getAllResult;
    static $paramtypesmap = array('getAllResult' => 'GetAllResult');
}