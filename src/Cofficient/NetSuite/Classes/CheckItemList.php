<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CheckItemList
 */
class CheckItemList
{
    /**
     * @access public
     * @var CheckItem[]
     */
    public $item;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('item' => 'CheckItem[]', 'replaceAll' => 'boolean');
}