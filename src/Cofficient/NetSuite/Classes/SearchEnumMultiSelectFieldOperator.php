<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchEnumMultiSelectFieldOperator
 */
class SearchEnumMultiSelectFieldOperator
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const anyOf = 'anyOf';
    /**
     * @var string
     */
    const noneOf = 'noneOf';
}