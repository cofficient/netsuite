<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetResponse
 */
class GetResponse
{
    /**
     * @access public
     * @var ReadResponse
     */
    public $readResponse;
    static $paramtypesmap = array('readResponse' => 'ReadResponse');
}