<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ReturnAuthorizationSalesTeamList
 */
class ReturnAuthorizationSalesTeamList
{
    /**
     * @access public
     * @var ReturnAuthorizationSalesTeam[]
     */
    public $salesTeam;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('salesTeam' => 'ReturnAuthorizationSalesTeam[]', 'replaceAll' => 'boolean');
}