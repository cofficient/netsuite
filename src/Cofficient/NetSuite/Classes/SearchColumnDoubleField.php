<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchColumnDoubleField
 */
class SearchColumnDoubleField extends SearchColumnField
{
    /**
     * @access public
     * @var float
     */
    public $searchValue;
    static $paramtypesmap = array('searchValue' => 'float');
}