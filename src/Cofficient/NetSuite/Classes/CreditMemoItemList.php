<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CreditMemoItemList
 */
class CreditMemoItemList
{
    /**
     * @access public
     * @var CreditMemoItem[]
     */
    public $item;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('item' => 'CreditMemoItem[]', 'replaceAll' => 'boolean');
}