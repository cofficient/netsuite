<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * RecordList
 */
class RecordList
{
    /**
     * @access public
     * @var Record[]
     */
    public $record;
    static $paramtypesmap = array('record' => 'Record[]');
}