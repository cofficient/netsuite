<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ExceededRecordCountFault
 */
class ExceededRecordCountFault extends NSSoapFault
{
    static $paramtypesmap = array();
}