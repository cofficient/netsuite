<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * Record
 */
class Record
{
    /**
     * @access public
     * @var NullField
     */
    public $nullFieldList;
    static $paramtypesmap = array('nullFieldList' => 'NullField');
}