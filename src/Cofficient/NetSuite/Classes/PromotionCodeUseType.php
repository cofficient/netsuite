<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * PromotionCodeUseType
 */
class PromotionCodeUseType
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _multipleUses = '_multipleUses';
    /**
     * @var string
     */
    const _singleUse = '_singleUse';
}