<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * UpdateRequest
 */
class UpdateRequest
{
    /**
     * @access public
     * @var Record
     */
    public $record;
    static $paramtypesmap = array('record' => 'Record');
}