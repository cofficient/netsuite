<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * DemandPlanCalendarType
 */
class DemandPlanCalendarType
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _daily = '_daily';
    /**
     * @var string
     */
    const _monthly = '_monthly';
    /**
     * @var string
     */
    const _weekly = '_weekly';
}