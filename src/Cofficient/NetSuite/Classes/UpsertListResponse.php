<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * UpsertListResponse
 */
class UpsertListResponse
{
    /**
     * @access public
     * @var WriteResponseList
     */
    public $writeResponseList;
    static $paramtypesmap = array('writeResponseList' => 'WriteResponseList');
}