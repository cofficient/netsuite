<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetDataCenterUrlsResponse
 */
class GetDataCenterUrlsResponse
{
    /**
     * @access public
     * @var GetDataCenterUrlsResult
     */
    public $getDataCenterUrlsResult;
    static $paramtypesmap = array('getDataCenterUrlsResult' => 'GetDataCenterUrlsResult');
}