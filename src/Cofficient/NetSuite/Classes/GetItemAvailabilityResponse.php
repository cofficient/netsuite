<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetItemAvailabilityResponse
 */
class GetItemAvailabilityResponse
{
    /**
     * @access public
     * @var GetItemAvailabilityResult
     */
    public $getItemAvailabilityResult;
    static $paramtypesmap = array('getItemAvailabilityResult' => 'GetItemAvailabilityResult');
}