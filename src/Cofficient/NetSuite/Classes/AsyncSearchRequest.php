<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AsyncSearchRequest
 */
class AsyncSearchRequest
{
    /**
     * @access public
     * @var SearchRecord
     */
    public $searchRecord;
    static $paramtypesmap = array('searchRecord' => 'SearchRecord');
}