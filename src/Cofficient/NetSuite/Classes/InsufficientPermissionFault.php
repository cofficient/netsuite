<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * InsufficientPermissionFault
 */
class InsufficientPermissionFault extends NSSoapFault
{
    static $paramtypesmap = array();
}