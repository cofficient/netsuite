<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchColumnBooleanField
 */
class SearchColumnBooleanField extends SearchColumnField
{
    /**
     * @access public
     * @var boolean
     */
    public $searchValue;
    static $paramtypesmap = array('searchValue' => 'boolean');
}