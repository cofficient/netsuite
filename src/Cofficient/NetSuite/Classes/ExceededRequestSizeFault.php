<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ExceededRequestSizeFault
 */
class ExceededRequestSizeFault extends NSSoapFault
{
    static $paramtypesmap = array();
}