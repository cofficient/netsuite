<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * StatusDetailType
 */
class StatusDetailType
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const ERROR = 'ERROR';
    /**
     * @var string
     */
    const WARN = 'WARN';
    /**
     * @var string
     */
    const INFO = 'INFO';
}