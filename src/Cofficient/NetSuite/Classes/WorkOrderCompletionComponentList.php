<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * WorkOrderCompletionComponentList
 */
class WorkOrderCompletionComponentList
{
    /**
     * @access public
     * @var WorkOrderCompletionComponent[]
     */
    public $workOrderCompletionComponent;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('workOrderCompletionComponent' => 'WorkOrderCompletionComponent[]', 'replaceAll' => 'boolean');
}