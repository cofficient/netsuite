<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ItemAccountingBookDetailList
 */
class ItemAccountingBookDetailList
{
    /**
     * @access public
     * @var ItemAccountingBookDetail[]
     */
    public $itemAccountingBookDetail;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('itemAccountingBookDetail' => 'ItemAccountingBookDetail[]', 'replaceAll' => 'boolean');
}