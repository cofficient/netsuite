<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchColumnDoubleCustomField
 */
class SearchColumnDoubleCustomField extends SearchColumnCustomField
{
    /**
     * @access public
     * @var float
     */
    public $searchValue;
    static $paramtypesmap = array('searchValue' => 'float');
}