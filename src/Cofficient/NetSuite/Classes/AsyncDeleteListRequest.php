<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AsyncDeleteListRequest
 */
class AsyncDeleteListRequest
{
    /**
     * @access public
     * @var BaseRef[]
     */
    public $baseRef;
    static $paramtypesmap = array('baseRef' => 'BaseRef[]');
}