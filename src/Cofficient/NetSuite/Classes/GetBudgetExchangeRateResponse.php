<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetBudgetExchangeRateResponse
 */
class GetBudgetExchangeRateResponse
{
    /**
     * @access public
     * @var GetBudgetExchangeRateResult
     */
    public $getBudgetExchangeRateResult;
    static $paramtypesmap = array('getBudgetExchangeRateResult' => 'GetBudgetExchangeRateResult');
}