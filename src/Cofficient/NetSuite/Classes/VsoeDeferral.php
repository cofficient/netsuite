<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * VsoeDeferral
 */
class VsoeDeferral
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _deferBundleUntilDelivered = '_deferBundleUntilDelivered';
    /**
     * @var string
     */
    const _deferUntilItemDelivered = '_deferUntilItemDelivered';
}