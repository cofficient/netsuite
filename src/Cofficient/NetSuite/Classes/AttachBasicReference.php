<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AttachBasicReference
 */
class AttachBasicReference extends AttachReference
{
    /**
     * @access public
     * @var BaseRef
     */
    public $attachedRecord;
    static $paramtypesmap = array('attachedRecord' => 'BaseRef');
}