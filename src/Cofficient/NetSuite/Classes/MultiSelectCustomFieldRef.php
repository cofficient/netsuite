<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * MultiSelectCustomFieldRef
 */
class MultiSelectCustomFieldRef extends CustomFieldRef
{
    /**
     * @access public
     * @var ListOrRecordRef[]
     */
    public $value;
    static $paramtypesmap = array('value' => 'ListOrRecordRef[]');
}