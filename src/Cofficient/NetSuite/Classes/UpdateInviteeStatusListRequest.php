<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * UpdateInviteeStatusListRequest
 */
class UpdateInviteeStatusListRequest
{
    /**
     * @access public
     * @var UpdateInviteeStatusReference[]
     */
    public $updateInviteeStatusReference;
    static $paramtypesmap = array('updateInviteeStatusReference' => 'UpdateInviteeStatusReference[]');
}