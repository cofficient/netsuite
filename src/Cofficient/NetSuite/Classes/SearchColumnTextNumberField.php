<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchColumnTextNumberField
 */
class SearchColumnTextNumberField extends SearchColumnField
{
    /**
     * @access public
     * @var string
     */
    public $searchValue;
    static $paramtypesmap = array('searchValue' => 'string');
}