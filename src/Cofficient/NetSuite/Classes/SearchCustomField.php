<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchCustomField
 */
class SearchCustomField
{
    /**
     * @access public
     * @var string
     */
    public $internalId;
    /**
     * @access public
     * @var string
     */
    public $scriptId;
    static $paramtypesmap = array('internalId' => 'string', 'scriptId' => 'string');
}