<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GroupMemberSearchBasic
 */
class GroupMemberSearchBasic extends SearchRecordBasic
{
    /**
     * @access public
     * @var SearchMultiSelectField
     */
    public $groupId;
    static $paramtypesmap = array('groupId' => 'SearchMultiSelectField');
}