<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CustomRecordTypeFieldList
 */
class CustomRecordTypeFieldList
{
    /**
     * @access public
     * @var CustomRecordCustomField[]
     */
    public $customField;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('customField' => 'CustomRecordCustomField[]', 'replaceAll' => 'boolean');
}