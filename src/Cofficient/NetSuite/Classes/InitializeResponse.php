<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * InitializeResponse
 */
class InitializeResponse
{
    /**
     * @access public
     * @var ReadResponse
     */
    public $readResponse;
    static $paramtypesmap = array('readResponse' => 'ReadResponse');
}