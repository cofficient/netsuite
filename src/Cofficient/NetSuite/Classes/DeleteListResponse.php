<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * DeleteListResponse
 */
class DeleteListResponse
{
    /**
     * @access public
     * @var WriteResponseList
     */
    public $writeResponseList;
    static $paramtypesmap = array('writeResponseList' => 'WriteResponseList');
}