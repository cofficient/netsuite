<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * InitializeListRequest
 */
class InitializeListRequest
{
    /**
     * @access public
     * @var InitializeRecord[]
     */
    public $initializeRecord;
    static $paramtypesmap = array('initializeRecord' => 'InitializeRecord[]');
}