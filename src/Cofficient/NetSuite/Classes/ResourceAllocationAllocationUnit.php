<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ResourceAllocationAllocationUnit
 */
class ResourceAllocationAllocationUnit
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _hours = '_hours';
    /**
     * @var string
     */
    const _percentOfTime = '_percentOfTime';
}