<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * TaxAcctType
 */
class TaxAcctType
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _sale = '_sale';
    /**
     * @var string
     */
    const _purchase = '_purchase';
}