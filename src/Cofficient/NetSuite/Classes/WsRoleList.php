<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * WsRoleList
 */
class WsRoleList
{
    /**
     * @access public
     * @var WsRole[]
     */
    public $wsRole;
    static $paramtypesmap = array('wsRole' => 'WsRole[]');
}