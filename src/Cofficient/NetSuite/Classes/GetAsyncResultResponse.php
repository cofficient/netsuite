<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetAsyncResultResponse
 */
class GetAsyncResultResponse
{
    /**
     * @access public
     * @var AsyncResult
     */
    public $asyncResult;
    static $paramtypesmap = array('asyncResult' => 'AsyncResult');
}