<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetBudgetExchangeRateRequest
 */
class GetBudgetExchangeRateRequest
{
    /**
     * @access public
     * @var BudgetExchangeRateFilter
     */
    public $budgetExchangeRateFilter;
    static $paramtypesmap = array('budgetExchangeRateFilter' => 'BudgetExchangeRateFilter');
}