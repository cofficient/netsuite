<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * TransactionColumnCustomFieldFilterList
 */
class TransactionColumnCustomFieldFilterList
{
    /**
     * @access public
     * @var TransactionColumnCustomFieldFilter[]
     */
    public $filter;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('filter' => 'TransactionColumnCustomFieldFilter[]', 'replaceAll' => 'boolean');
}