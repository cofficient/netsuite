<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CalendarEventAttendeeAttendance
 */
class CalendarEventAttendeeAttendance
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _optional = '_optional';
    /**
     * @var string
     */
    const _required = '_required';
}