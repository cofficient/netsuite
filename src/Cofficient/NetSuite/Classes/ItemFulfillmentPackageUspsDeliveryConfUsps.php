<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ItemFulfillmentPackageUspsDeliveryConfUsps
 */
class ItemFulfillmentPackageUspsDeliveryConfUsps
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _deliveryConfirmation = '_deliveryConfirmation';
    /**
     * @var string
     */
    const _signatureConfirmation = '_signatureConfirmation';
}