<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * EmailEmployeesList
 */
class EmailEmployeesList
{
    /**
     * @access public
     * @var RecordRef[]
     */
    public $emailEmployees;
    static $paramtypesmap = array('emailEmployees' => 'RecordRef[]');
}