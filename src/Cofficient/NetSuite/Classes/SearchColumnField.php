<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchColumnField
 */
class SearchColumnField
{
    /**
     * @access public
     * @var string
     */
    public $customLabel;
    static $paramtypesmap = array('customLabel' => 'string');
}