<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetSavedSearchRecord
 */
class GetSavedSearchRecord
{
    /**
     * @access public
     * @var SearchRecordType
     */
    public $searchType;
    static $paramtypesmap = array('searchType' => 'SearchRecordType');
}