<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CurrencyCurrencyPrecision
 */
class CurrencyCurrencyPrecision
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _one = '_one';
    /**
     * @var string
     */
    const _two = '_two';
    /**
     * @var string
     */
    const _zero = '_zero';
}