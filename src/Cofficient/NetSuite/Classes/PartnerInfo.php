<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * PartnerInfo
 */
class PartnerInfo
{
    /**
     * @access public
     * @var string
     */
    public $partnerId;
    static $paramtypesmap = array('partnerId' => 'string');
}