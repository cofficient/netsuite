<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CheckAsyncStatusRequest
 */
class CheckAsyncStatusRequest
{
    /**
     * @access public
     * @var string
     */
    public $jobId;
    static $paramtypesmap = array('jobId' => 'string');
}