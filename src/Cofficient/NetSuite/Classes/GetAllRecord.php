<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetAllRecord
 */
class GetAllRecord
{
    /**
     * @access public
     * @var GetAllRecordType
     */
    public $recordType;
    static $paramtypesmap = array('recordType' => 'GetAllRecordType');
}