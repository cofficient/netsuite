<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AsyncGetListResult
 */
class AsyncGetListResult extends AsyncResult
{
    /**
     * @access public
     * @var ReadResponseList
     */
    public $readResponseList;
    static $paramtypesmap = array('readResponseList' => 'ReadResponseList');
}