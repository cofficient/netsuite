<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * DetachResponse
 */
class DetachResponse
{
    /**
     * @access public
     * @var WriteResponse
     */
    public $writeResponse;
    static $paramtypesmap = array('writeResponse' => 'WriteResponse');
}