<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * MapSsoRequest
 */
class MapSsoRequest
{
    /**
     * @access public
     * @var SsoCredentials
     */
    public $ssoCredentials;
    static $paramtypesmap = array('ssoCredentials' => 'SsoCredentials');
}