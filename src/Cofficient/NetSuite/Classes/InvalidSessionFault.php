<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * InvalidSessionFault
 */
class InvalidSessionFault extends NSSoapFault
{
    static $paramtypesmap = array();
}