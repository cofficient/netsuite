<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ItemEbayRelistingOption
 */
class ItemEbayRelistingOption
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _doNotRelist = '_doNotRelist';
    /**
     * @var string
     */
    const _relistWhenItemExpires = '_relistWhenItemExpires';
    /**
     * @var string
     */
    const _relistWhenItemIsSold = '_relistWhenItemIsSold';
    /**
     * @var string
     */
    const _relistWhenItemIsSoldExpires = '_relistWhenItemIsSoldExpires';
}