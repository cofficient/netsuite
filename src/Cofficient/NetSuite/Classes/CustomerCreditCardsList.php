<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CustomerCreditCardsList
 */
class CustomerCreditCardsList
{
    /**
     * @access public
     * @var CustomerCreditCards[]
     */
    public $creditCards;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('creditCards' => 'CustomerCreditCards[]', 'replaceAll' => 'boolean');
}