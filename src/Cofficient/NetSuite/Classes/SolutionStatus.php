<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SolutionStatus
 */
class SolutionStatus
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _approved = '_approved';
    /**
     * @var string
     */
    const _unapproved = '_unapproved';
}