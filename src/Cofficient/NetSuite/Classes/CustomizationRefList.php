<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CustomizationRefList
 */
class CustomizationRefList
{
    /**
     * @access public
     * @var CustomizationRef[]
     */
    public $customizationRef;
    static $paramtypesmap = array('customizationRef' => 'CustomizationRef[]');
}