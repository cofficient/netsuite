<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * Solutions
 */
class Solutions
{
    /**
     * @access public
     * @var RecordRef
     */
    public $solution;
    /**
     * @access public
     * @var string
     */
    public $message;
    static $paramtypesmap = array('solution' => 'RecordRef', 'message' => 'string');
}