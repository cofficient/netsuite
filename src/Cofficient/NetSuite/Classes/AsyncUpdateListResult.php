<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AsyncUpdateListResult
 */
class AsyncUpdateListResult extends AsyncResult
{
    /**
     * @access public
     * @var WriteResponseList
     */
    public $writeResponseList;
    static $paramtypesmap = array('writeResponseList' => 'WriteResponseList');
}