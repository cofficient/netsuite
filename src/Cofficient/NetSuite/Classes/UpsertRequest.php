<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * UpsertRequest
 */
class UpsertRequest
{
    /**
     * @access public
     * @var Record
     */
    public $record;
    static $paramtypesmap = array('record' => 'Record');
}