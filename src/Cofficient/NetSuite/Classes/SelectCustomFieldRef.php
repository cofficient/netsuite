<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SelectCustomFieldRef
 */
class SelectCustomFieldRef extends CustomFieldRef
{
    /**
     * @access public
     * @var ListOrRecordRef
     */
    public $value;
    static $paramtypesmap = array('value' => 'ListOrRecordRef');
}