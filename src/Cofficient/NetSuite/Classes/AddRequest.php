<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AddRequest
 */
class AddRequest
{
    /**
     * @access public
     * @var Record
     */
    public $record;
    static $paramtypesmap = array('record' => 'Record');
}