<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * DurationUnit
 */
class DurationUnit
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const hour = 'hour';
}