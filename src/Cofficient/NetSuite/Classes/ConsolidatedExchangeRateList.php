<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ConsolidatedExchangeRateList
 */
class ConsolidatedExchangeRateList
{
    /**
     * @access public
     * @var ConsolidatedExchangeRate[]
     */
    public $consolidatedExchangeRate;
    static $paramtypesmap = array('consolidatedExchangeRate' => 'ConsolidatedExchangeRate[]');
}