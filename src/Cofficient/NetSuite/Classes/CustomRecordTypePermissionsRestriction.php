<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CustomRecordTypePermissionsRestriction
 */
class CustomRecordTypePermissionsRestriction
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _editingOnly = '_editingOnly';
    /**
     * @var string
     */
    const _viewingAndEditing = '_viewingAndEditing';
}