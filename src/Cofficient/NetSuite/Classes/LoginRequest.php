<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * LoginRequest
 */
class LoginRequest
{
    /**
     * @access public
     * @var Passport
     */
    public $passport;
    static $paramtypesmap = array('passport' => 'Passport');
}