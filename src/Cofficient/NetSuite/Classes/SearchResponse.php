<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchResponse
 */
class SearchResponse
{
    /**
     * @access public
     * @var SearchResult
     */
    public $searchResult;
    static $paramtypesmap = array('searchResult' => 'SearchResult');
}