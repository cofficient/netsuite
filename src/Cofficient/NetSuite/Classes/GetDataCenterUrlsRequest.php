<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetDataCenterUrlsRequest
 */
class GetDataCenterUrlsRequest
{
    /**
     * @access public
     * @var string
     */
    public $account;
    static $paramtypesmap = array('account' => 'string');
}