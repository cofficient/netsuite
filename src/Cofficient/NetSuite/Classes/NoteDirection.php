<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * NoteDirection
 */
class NoteDirection
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _incoming = '_incoming';
    /**
     * @var string
     */
    const _outgoing = '_outgoing';
}