<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ItemFulfillmentPackageUpsCodMethodUps
 */
class ItemFulfillmentPackageUpsCodMethodUps
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _cashiersCheckMoneyOrder = '_cashiersCheckMoneyOrder';
    /**
     * @var string
     */
    const _checkCashiersCheckMoneyOrder = '_checkCashiersCheckMoneyOrder';
}