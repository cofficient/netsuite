<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * EmployeeDeductionList
 */
class EmployeeDeductionList
{
    /**
     * @access public
     * @var EmployeeDeduction[]
     */
    public $employeeDeduction;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('employeeDeduction' => 'EmployeeDeduction[]', 'replaceAll' => 'boolean');
}