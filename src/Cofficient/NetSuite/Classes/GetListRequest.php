<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetListRequest
 */
class GetListRequest
{
    /**
     * @access public
     * @var BaseRef[]
     */
    public $baseRef;
    static $paramtypesmap = array('baseRef' => 'BaseRef[]');
}