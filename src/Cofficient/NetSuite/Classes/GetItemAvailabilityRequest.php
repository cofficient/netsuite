<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetItemAvailabilityRequest
 */
class GetItemAvailabilityRequest
{
    /**
     * @access public
     * @var ItemAvailabilityFilter
     */
    public $itemAvailabilityFilter;
    static $paramtypesmap = array('itemAvailabilityFilter' => 'ItemAvailabilityFilter');
}