<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchMoreResponse
 */
class SearchMoreResponse
{
    /**
     * @access public
     * @var SearchResult
     */
    public $searchResult;
    static $paramtypesmap = array('searchResult' => 'SearchResult');
}