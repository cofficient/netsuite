<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CalendarEventResource
 */
class CalendarEventResource
{
    /**
     * @access public
     * @var RecordRef
     */
    public $resource;
    /**
     * @access public
     * @var string
     */
    public $location;
    static $paramtypesmap = array('resource' => 'RecordRef', 'location' => 'string');
}