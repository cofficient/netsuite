<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ReadResponse
 */
class ReadResponse
{
    /**
     * @access public
     * @var Status
     */
    public $status;
    /**
     * @access public
     * @var Record
     */
    public $record;
    static $paramtypesmap = array('status' => 'Status', 'record' => 'Record');
}