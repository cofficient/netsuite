<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetConsolidatedExchangeRateRequest
 */
class GetConsolidatedExchangeRateRequest
{
    /**
     * @access public
     * @var ConsolidatedExchangeRateFilter
     */
    public $consolidatedExchangeRateFilter;
    static $paramtypesmap = array('consolidatedExchangeRateFilter' => 'ConsolidatedExchangeRateFilter');
}