<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchNextResponse
 */
class SearchNextResponse
{
    /**
     * @access public
     * @var SearchResult
     */
    public $searchResult;
    static $paramtypesmap = array('searchResult' => 'SearchResult');
}