<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetDeletedResponse
 */
class GetDeletedResponse
{
    /**
     * @access public
     * @var GetDeletedResult
     */
    public $getDeletedResult;
    static $paramtypesmap = array('getDeletedResult' => 'GetDeletedResult');
}