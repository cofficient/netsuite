<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * InvalidCredentialsFault
 */
class InvalidCredentialsFault extends NSSoapFault
{
    static $paramtypesmap = array();
}