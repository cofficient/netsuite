<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AccountingPeriodFiscalCalendars
 */
class AccountingPeriodFiscalCalendars
{
    /**
     * @access public
     * @var RecordRef
     */
    public $fiscalCalendar;
    /**
     * @access public
     * @var RecordRef
     */
    public $parent;
    static $paramtypesmap = array('fiscalCalendar' => 'RecordRef', 'parent' => 'RecordRef');
}