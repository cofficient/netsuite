<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * DocumentInfo
 */
class DocumentInfo
{
    /**
     * @access public
     * @var string
     */
    public $nsId;
    static $paramtypesmap = array('nsId' => 'string');
}