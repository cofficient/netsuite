<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ProductFeedList
 */
class ProductFeedList
{
    /**
     * @access public
     * @var ItemProductFeed[]
     */
    public $productFeed;
    static $paramtypesmap = array('productFeed' => 'ItemProductFeed[]');
}