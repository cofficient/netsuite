<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AsyncInitializeListRequest
 */
class AsyncInitializeListRequest
{
    /**
     * @access public
     * @var InitializeRecord[]
     */
    public $initializeRecord;
    static $paramtypesmap = array('initializeRecord' => 'InitializeRecord[]');
}