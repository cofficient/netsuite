<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchRequest
 */
class SearchRequest
{
    /**
     * @access public
     * @var SearchRecord
     */
    public $searchRecord;
    static $paramtypesmap = array('searchRecord' => 'SearchRecord');
}