<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetCurrencyRateResponse
 */
class GetCurrencyRateResponse
{
    /**
     * @access public
     * @var GetCurrencyRateResult
     */
    public $getCurrencyRateResult;
    static $paramtypesmap = array('getCurrencyRateResult' => 'GetCurrencyRateResult');
}