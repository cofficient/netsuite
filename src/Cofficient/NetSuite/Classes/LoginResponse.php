<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * LoginResponse
 */
class LoginResponse
{
    /**
     * @access public
     * @var SessionResponse
     */
    public $sessionResponse;
    static $paramtypesmap = array('sessionResponse' => 'SessionResponse');
}