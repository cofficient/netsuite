<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * TimeItemTimeType
 */
class TimeItemTimeType
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _actualTime = '_actualTime';
    /**
     * @var string
     */
    const _plannedTime = '_plannedTime';
}