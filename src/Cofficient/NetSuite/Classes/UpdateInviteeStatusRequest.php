<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * UpdateInviteeStatusRequest
 */
class UpdateInviteeStatusRequest
{
    /**
     * @access public
     * @var UpdateInviteeStatusReference
     */
    public $updateInviteeStatusReference;
    static $paramtypesmap = array('updateInviteeStatusReference' => 'UpdateInviteeStatusReference');
}