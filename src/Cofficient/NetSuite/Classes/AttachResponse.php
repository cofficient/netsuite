<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AttachResponse
 */
class AttachResponse
{
    /**
     * @access public
     * @var WriteResponse
     */
    public $writeResponse;
    static $paramtypesmap = array('writeResponse' => 'WriteResponse');
}