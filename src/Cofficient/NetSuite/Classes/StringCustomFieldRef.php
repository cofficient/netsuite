<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * StringCustomFieldRef
 */
class StringCustomFieldRef extends CustomFieldRef
{
    /**
     * @access public
     * @var string
     */
    public $value;
    static $paramtypesmap = array('value' => 'string');
}