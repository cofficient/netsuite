<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchEnumMultiSelectField
 */
class SearchEnumMultiSelectField
{
    /**
     * @access public
     * @var string[]
     */
    public $searchValue;
    /**
     * @access public
     * @var SearchEnumMultiSelectFieldOperator
     */
    public $operator;
    static $paramtypesmap = array('searchValue' => 'string[]', 'operator' => 'SearchEnumMultiSelectFieldOperator');
}