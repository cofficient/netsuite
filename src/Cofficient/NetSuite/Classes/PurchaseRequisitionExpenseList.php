<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * PurchaseRequisitionExpenseList
 */
class PurchaseRequisitionExpenseList
{
    /**
     * @access public
     * @var PurchaseRequisitionExpense[]
     */
    public $purchaseRequisitionExpense;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('purchaseRequisitionExpense' => 'PurchaseRequisitionExpense[]', 'replaceAll' => 'boolean');
}