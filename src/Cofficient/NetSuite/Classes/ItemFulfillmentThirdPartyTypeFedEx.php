<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ItemFulfillmentThirdPartyTypeFedEx
 */
class ItemFulfillmentThirdPartyTypeFedEx
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _billRecipient = '_billRecipient';
    /**
     * @var string
     */
    const _billThirdParty = '_billThirdParty';
    /**
     * @var string
     */
    const _noneSelected = '_noneSelected';
}