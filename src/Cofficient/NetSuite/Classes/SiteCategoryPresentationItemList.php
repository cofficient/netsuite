<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SiteCategoryPresentationItemList
 */
class SiteCategoryPresentationItemList
{
    /**
     * @access public
     * @var PresentationItem[]
     */
    public $presentationItem;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('presentationItem' => 'PresentationItem[]', 'replaceAll' => 'boolean');
}