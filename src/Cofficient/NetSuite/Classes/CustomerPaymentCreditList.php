<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CustomerPaymentCreditList
 */
class CustomerPaymentCreditList
{
    /**
     * @access public
     * @var CustomerPaymentCredit[]
     */
    public $credit;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('credit' => 'CustomerPaymentCredit[]', 'replaceAll' => 'boolean');
}