<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * PromotionCodeApplyDiscountTo
 */
class PromotionCodeApplyDiscountTo
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _allSales = '_allSales';
    /**
     * @var string
     */
    const _firstSaleOnly = '_firstSaleOnly';
}