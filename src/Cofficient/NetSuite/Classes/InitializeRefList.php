<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * InitializeRefList
 */
class InitializeRefList
{
    /**
     * @access public
     * @var InitializeRef[]
     */
    public $initializeRef;
    static $paramtypesmap = array('initializeRef' => 'InitializeRef[]');
}