<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ItemsList
 */
class ItemsList
{
    /**
     * @access public
     * @var RecordRef[]
     */
    public $items;
    static $paramtypesmap = array('items' => 'RecordRef[]');
}