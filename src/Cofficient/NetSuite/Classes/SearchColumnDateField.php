<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchColumnDateField
 */
class SearchColumnDateField extends SearchColumnField
{
    /**
     * @access public
     * @var dateTime
     */
    public $searchValue;
    static $paramtypesmap = array('searchValue' => 'dateTime');
}