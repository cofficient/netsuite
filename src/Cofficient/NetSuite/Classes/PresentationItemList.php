<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * PresentationItemList
 */
class PresentationItemList
{
    /**
     * @access public
     * @var PresentationItem[]
     */
    public $presentationItem;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('presentationItem' => 'PresentationItem[]', 'replaceAll' => 'boolean');
}