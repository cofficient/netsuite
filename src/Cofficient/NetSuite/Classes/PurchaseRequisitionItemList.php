<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * PurchaseRequisitionItemList
 */
class PurchaseRequisitionItemList
{
    /**
     * @access public
     * @var PurchaseRequisitionItem[]
     */
    public $purchaseRequisitionItem;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('purchaseRequisitionItem' => 'PurchaseRequisitionItem[]', 'replaceAll' => 'boolean');
}