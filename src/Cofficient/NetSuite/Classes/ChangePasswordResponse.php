<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ChangePasswordResponse
 */
class ChangePasswordResponse
{
    /**
     * @access public
     * @var SessionResponse
     */
    public $sessionResponse;
    static $paramtypesmap = array('sessionResponse' => 'SessionResponse');
}