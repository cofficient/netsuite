<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchRowList
 */
class SearchRowList
{
    /**
     * @access public
     * @var SearchRow[]
     */
    public $searchRow;
    static $paramtypesmap = array('searchRow' => 'SearchRow[]');
}