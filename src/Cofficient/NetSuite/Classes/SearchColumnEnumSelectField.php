<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchColumnEnumSelectField
 */
class SearchColumnEnumSelectField extends SearchColumnField
{
    /**
     * @access public
     * @var string
     */
    public $searchValue;
    static $paramtypesmap = array('searchValue' => 'string');
}