<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchColumnCustomFieldList
 */
class SearchColumnCustomFieldList
{
    /**
     * @access public
     * @var SearchColumnCustomField[]
     */
    public $customField;
    static $paramtypesmap = array('customField' => 'SearchColumnCustomField[]');
}