<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AttachReference
 */
class AttachReference
{
    /**
     * @access public
     * @var BaseRef
     */
    public $attachTo;
    static $paramtypesmap = array('attachTo' => 'BaseRef');
}