<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AsyncAddListRequest
 */
class AsyncAddListRequest
{
    /**
     * @access public
     * @var Record[]
     */
    public $record;
    static $paramtypesmap = array('record' => 'Record[]');
}