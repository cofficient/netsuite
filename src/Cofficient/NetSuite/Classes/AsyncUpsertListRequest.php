<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AsyncUpsertListRequest
 */
class AsyncUpsertListRequest
{
    /**
     * @access public
     * @var Record[]
     */
    public $record;
    static $paramtypesmap = array('record' => 'Record[]');
}