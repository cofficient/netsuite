<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CustomSearchRowBasic
 */
class CustomSearchRowBasic
{
    /**
     * @access public
     * @var CustomizationRef
     */
    public $customizationRef;
    /**
     * @access public
     * @var SearchRowBasic
     */
    public $searchRowBasic;
    static $paramtypesmap = array('customizationRef' => 'CustomizationRef', 'searchRowBasic' => 'SearchRowBasic');
}