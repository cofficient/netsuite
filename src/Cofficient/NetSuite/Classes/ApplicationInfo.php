<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ApplicationInfo
 */
class ApplicationInfo
{
    /**
     * @access public
     * @var string
     */
    public $applicationId;
    static $paramtypesmap = array('applicationId' => 'string');
}