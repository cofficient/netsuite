<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SupportCaseEscalateToList
 */
class SupportCaseEscalateToList
{
    /**
     * @access public
     * @var SupportCaseEscalateTo[]
     */
    public $escalateTo;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('escalateTo' => 'SupportCaseEscalateTo[]', 'replaceAll' => 'boolean');
}