<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * TransactionBodyCustomFieldFilterList
 */
class TransactionBodyCustomFieldFilterList
{
    /**
     * @access public
     * @var TransactionBodyCustomFieldFilter[]
     */
    public $filter;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('filter' => 'TransactionBodyCustomFieldFilter[]', 'replaceAll' => 'boolean');
}