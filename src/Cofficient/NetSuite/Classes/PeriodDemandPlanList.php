<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * PeriodDemandPlanList
 */
class PeriodDemandPlanList
{
    /**
     * @access public
     * @var PeriodDemandPlan[]
     */
    public $periodDemandPlan;
    static $paramtypesmap = array('periodDemandPlan' => 'PeriodDemandPlan[]');
}