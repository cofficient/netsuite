<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * FileAttachFrom
 */
class FileAttachFrom
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _computer = '_computer';
    /**
     * @var string
     */
    const _web = '_web';
}