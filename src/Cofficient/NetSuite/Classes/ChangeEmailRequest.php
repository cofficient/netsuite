<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ChangeEmailRequest
 */
class ChangeEmailRequest
{
    /**
     * @access public
     * @var ChangeEmail
     */
    public $changeEmail;
    static $paramtypesmap = array('changeEmail' => 'ChangeEmail');
}