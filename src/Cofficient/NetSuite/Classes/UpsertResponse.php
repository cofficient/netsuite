<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * UpsertResponse
 */
class UpsertResponse
{
    /**
     * @access public
     * @var WriteResponse
     */
    public $writeResponse;
    static $paramtypesmap = array('writeResponse' => 'WriteResponse');
}