<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * UpdateResponse
 */
class UpdateResponse
{
    /**
     * @access public
     * @var WriteResponse
     */
    public $writeResponse;
    static $paramtypesmap = array('writeResponse' => 'WriteResponse');
}