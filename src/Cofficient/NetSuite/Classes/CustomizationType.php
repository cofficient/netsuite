<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CustomizationType
 */
class CustomizationType
{
    /**
     * @access public
     * @var GetCustomizationType
     */
    public $getCustomizationType;
    static $paramtypesmap = array('getCustomizationType' => 'GetCustomizationType');
}