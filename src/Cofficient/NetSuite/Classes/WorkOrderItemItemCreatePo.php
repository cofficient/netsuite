<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * WorkOrderItemItemCreatePo
 */
class WorkOrderItemItemCreatePo
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _dropShipment = '_dropShipment';
    /**
     * @var string
     */
    const _specialOrder = '_specialOrder';
}