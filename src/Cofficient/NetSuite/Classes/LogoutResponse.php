<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * LogoutResponse
 */
class LogoutResponse
{
    /**
     * @access public
     * @var SessionResponse
     */
    public $sessionResponse;
    static $paramtypesmap = array('sessionResponse' => 'SessionResponse');
}