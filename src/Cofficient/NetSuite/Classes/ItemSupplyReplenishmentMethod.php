<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ItemSupplyReplenishmentMethod
 */
class ItemSupplyReplenishmentMethod
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _reorderPoint = '_reorderPoint';
    /**
     * @var string
     */
    const _timePhased = '_timePhased';
}