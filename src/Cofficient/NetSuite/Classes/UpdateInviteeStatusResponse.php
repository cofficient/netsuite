<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * UpdateInviteeStatusResponse
 */
class UpdateInviteeStatusResponse
{
    /**
     * @access public
     * @var WriteResponse
     */
    public $writeResponse;
    static $paramtypesmap = array('writeResponse' => 'WriteResponse');
}