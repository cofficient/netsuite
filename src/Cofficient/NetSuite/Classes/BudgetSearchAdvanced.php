<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * BudgetSearchAdvanced
 */
class BudgetSearchAdvanced extends SearchRecord
{
    /**
     * @access public
     * @var BudgetSearch
     */
    public $criteria;
    /**
     * @access public
     * @var BudgetSearchRow
     */
    public $columns;
    /**
     * @access public
     * @var string
     */
    public $savedSearchId;
    /**
     * @access public
     * @var string
     */
    public $savedSearchScriptId;
    static $paramtypesmap = array('criteria' => 'BudgetSearch', 'columns' => 'BudgetSearchRow', 'savedSearchId' => 'string', 'savedSearchScriptId' => 'string');
}