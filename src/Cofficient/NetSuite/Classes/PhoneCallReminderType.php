<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * PhoneCallReminderType
 */
class PhoneCallReminderType
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _eMail = '_eMail';
    /**
     * @var string
     */
    const _popupWindow = '_popupWindow';
}