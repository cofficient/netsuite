<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchBooleanField
 */
class SearchBooleanField
{
    /**
     * @access public
     * @var boolean
     */
    public $searchValue;
    static $paramtypesmap = array('searchValue' => 'boolean');
}