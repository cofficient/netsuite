<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ItemAvailabilityList
 */
class ItemAvailabilityList
{
    /**
     * @access public
     * @var ItemAvailability[]
     */
    public $itemAvailability;
    static $paramtypesmap = array('itemAvailability' => 'ItemAvailability[]');
}