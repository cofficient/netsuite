<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * PricingGroupSearchRow
 */
class PricingGroupSearchRow extends SearchRow
{
    /**
     * @access public
     * @var PricingGroupSearchRowBasic
     */
    public $basic;
    /**
     * @access public
     * @var EmployeeSearchRowBasic
     */
    public $userJoin;
    static $paramtypesmap = array('basic' => 'PricingGroupSearchRowBasic', 'userJoin' => 'EmployeeSearchRowBasic');
}