<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetSelectValueFilterOperator
 */
class GetSelectValueFilterOperator
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const contains = 'contains';
    /**
     * @var string
     */
    const is = 'is';
    /**
     * @var string
     */
    const startsWith = 'startsWith';
}