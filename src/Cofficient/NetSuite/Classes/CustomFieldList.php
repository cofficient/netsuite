<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CustomFieldList
 */
class CustomFieldList
{
    /**
     * @access public
     * @var CustomFieldRef[]
     */
    public $customField;
    static $paramtypesmap = array('customField' => 'CustomFieldRef[]');
}