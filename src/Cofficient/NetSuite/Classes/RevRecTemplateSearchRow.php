<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * RevRecTemplateSearchRow
 */
class RevRecTemplateSearchRow extends SearchRow
{
    /**
     * @access public
     * @var RevRecTemplateSearchRowBasic
     */
    public $basic;
    /**
     * @access public
     * @var EmployeeSearchRowBasic
     */
    public $userJoin;
    static $paramtypesmap = array('basic' => 'RevRecTemplateSearchRowBasic', 'userJoin' => 'EmployeeSearchRowBasic');
}