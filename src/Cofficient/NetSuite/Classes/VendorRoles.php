<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * VendorRoles
 */
class VendorRoles
{
    /**
     * @access public
     * @var RecordRef
     */
    public $selectedRole;
    static $paramtypesmap = array('selectedRole' => 'RecordRef');
}