<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CampaignDirectMailList
 */
class CampaignDirectMailList
{
    /**
     * @access public
     * @var CampaignDirectMail[]
     */
    public $campaignDirectMail;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('campaignDirectMail' => 'CampaignDirectMail[]', 'replaceAll' => 'boolean');
}