<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * UpdateListResponse
 */
class UpdateListResponse
{
    /**
     * @access public
     * @var WriteResponseList
     */
    public $writeResponseList;
    static $paramtypesmap = array('writeResponseList' => 'WriteResponseList');
}