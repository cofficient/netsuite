<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchMoreRequest
 */
class SearchMoreRequest
{
    /**
     * @access public
     * @var integer
     */
    public $pageIndex;
    static $paramtypesmap = array('pageIndex' => 'integer');
}