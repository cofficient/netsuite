<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * MatrixOptionList
 */
class MatrixOptionList
{
    /**
     * @access public
     * @var SelectCustomFieldRef[]
     */
    public $matrixOption;
    static $paramtypesmap = array('matrixOption' => 'SelectCustomFieldRef[]');
}