<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * PostingTransactionSummaryList
 */
class PostingTransactionSummaryList
{
    /**
     * @access public
     * @var PostingTransactionSummary[]
     */
    public $postingTransactionSummary;
    static $paramtypesmap = array('postingTransactionSummary' => 'PostingTransactionSummary[]');
}