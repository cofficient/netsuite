<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetSavedSearchRequest
 */
class GetSavedSearchRequest
{
    /**
     * @access public
     * @var GetSavedSearchRecord
     */
    public $record;
    static $paramtypesmap = array('record' => 'GetSavedSearchRecord');
}