<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * UpsertListRequest
 */
class UpsertListRequest
{
    /**
     * @access public
     * @var Record[]
     */
    public $record;
    static $paramtypesmap = array('record' => 'Record[]');
}