<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * TaskReminderType
 */
class TaskReminderType
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _eMail = '_eMail';
    /**
     * @var string
     */
    const _popupWindow = '_popupWindow';
}