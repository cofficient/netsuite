<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ProjectTaskPredecessorList
 */
class ProjectTaskPredecessorList
{
    /**
     * @access public
     * @var ProjectTaskPredecessor[]
     */
    public $projectTaskPredecessor;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('projectTaskPredecessor' => 'ProjectTaskPredecessor[]', 'replaceAll' => 'boolean');
}