<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetServerTimeResponse
 */
class GetServerTimeResponse
{
    /**
     * @access public
     * @var GetServerTimeResult
     */
    public $getServerTimeResult;
    static $paramtypesmap = array('getServerTimeResult' => 'GetServerTimeResult');
}