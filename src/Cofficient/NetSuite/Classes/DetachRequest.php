<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * DetachRequest
 */
class DetachRequest
{
    /**
     * @access public
     * @var DetachReference
     */
    public $detachReference;
    static $paramtypesmap = array('detachReference' => 'DetachReference');
}