<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * EmployeeCommissionPaymentPreference
 */
class EmployeeCommissionPaymentPreference
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _accountsPayable = '_accountsPayable';
    /**
     * @var string
     */
    const _payroll = '_payroll';
    /**
     * @var string
     */
    const _systemPreference = '_systemPreference';
}