<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * UpdateListRequest
 */
class UpdateListRequest
{
    /**
     * @access public
     * @var Record[]
     */
    public $record;
    static $paramtypesmap = array('record' => 'Record[]');
}