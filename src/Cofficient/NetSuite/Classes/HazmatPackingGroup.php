<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * HazmatPackingGroup
 */
class HazmatPackingGroup
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _i = '_i';
    /**
     * @var string
     */
    const _ii = '_ii';
    /**
     * @var string
     */
    const _iii = '_iii';
}