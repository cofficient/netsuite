<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetConsolidatedExchangeRateResponse
 */
class GetConsolidatedExchangeRateResponse
{
    /**
     * @access public
     * @var GetConsolidatedExchangeRateResult
     */
    public $getConsolidatedExchangeRateResult;
    static $paramtypesmap = array('getConsolidatedExchangeRateResult' => 'GetConsolidatedExchangeRateResult');
}