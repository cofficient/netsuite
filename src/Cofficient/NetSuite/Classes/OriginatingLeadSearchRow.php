<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * OriginatingLeadSearchRow
 */
class OriginatingLeadSearchRow extends SearchRow
{
    /**
     * @access public
     * @var OriginatingLeadSearchRowBasic
     */
    public $basic;
    /**
     * @access public
     * @var CustomSearchRowBasic[]
     */
    public $customSearchJoin;
    static $paramtypesmap = array('basic' => 'OriginatingLeadSearchRowBasic', 'customSearchJoin' => 'CustomSearchRowBasic[]');
}