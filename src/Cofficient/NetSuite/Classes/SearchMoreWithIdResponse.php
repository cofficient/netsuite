<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchMoreWithIdResponse
 */
class SearchMoreWithIdResponse
{
    /**
     * @access public
     * @var SearchResult
     */
    public $searchResult;
    static $paramtypesmap = array('searchResult' => 'SearchResult');
}