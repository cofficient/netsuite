<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ItemFulfillmentPackageFedExDeliveryConfFedEx
 */
class ItemFulfillmentPackageFedExDeliveryConfFedEx
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _signatureRequired = '_signatureRequired';
}