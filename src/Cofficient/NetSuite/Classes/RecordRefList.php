<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * RecordRefList
 */
class RecordRefList
{
    /**
     * @access public
     * @var RecordRef[]
     */
    public $recordRef;
    static $paramtypesmap = array('recordRef' => 'RecordRef[]');
}