<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetListResponse
 */
class GetListResponse
{
    /**
     * @access public
     * @var ReadResponseList
     */
    public $readResponseList;
    static $paramtypesmap = array('readResponseList' => 'ReadResponseList');
}