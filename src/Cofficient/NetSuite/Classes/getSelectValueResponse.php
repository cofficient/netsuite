<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * getSelectValueResponse
 */
class getSelectValueResponse
{
    /**
     * @access public
     * @var GetSelectValueResult
     */
    public $getSelectValueResult;
    static $paramtypesmap = array('getSelectValueResult' => 'GetSelectValueResult');
}