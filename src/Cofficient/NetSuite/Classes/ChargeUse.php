<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ChargeUse
 */
class ChargeUse
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _actual = '_actual';
    /**
     * @var string
     */
    const _forecast = '_forecast';
}