<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * BillingScheduleSearchRow
 */
class BillingScheduleSearchRow extends SearchRow
{
    /**
     * @access public
     * @var BillingScheduleSearchRowBasic
     */
    public $basic;
    static $paramtypesmap = array('basic' => 'BillingScheduleSearchRowBasic');
}