<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * BooleanCustomFieldRef
 */
class BooleanCustomFieldRef extends CustomFieldRef
{
    /**
     * @access public
     * @var boolean
     */
    public $value;
    static $paramtypesmap = array('value' => 'boolean');
}