<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AccountTranslationList
 */
class AccountTranslationList
{
    /**
     * @access public
     * @var ClassTranslation[]
     */
    public $translation;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('translation' => 'ClassTranslation[]', 'replaceAll' => 'boolean');
}