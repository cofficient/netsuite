<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * EmployeeDirectDepositList
 */
class EmployeeDirectDepositList
{
    /**
     * @access public
     * @var EmployeeDirectDeposit[]
     */
    public $employeeDirectDeposit;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('employeeDirectDeposit' => 'EmployeeDirectDeposit[]', 'replaceAll' => 'boolean');
}