<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * BaseRef
 */
class BaseRef
{
    /**
     * @access public
     * @var string
     */
    public $name;
    static $paramtypesmap = array('name' => 'string');
}