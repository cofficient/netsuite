<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * WriteResponseList
 */
class WriteResponseList
{
    /**
     * @access public
     * @var Status
     */
    public $status;
    /**
     * @access public
     * @var WriteResponse[]
     */
    public $writeResponse;
    static $paramtypesmap = array('status' => 'Status', 'writeResponse' => 'WriteResponse[]');
}