<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * WorkOrderSchedulingMethod
 */
class WorkOrderSchedulingMethod
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _backward = '_backward';
    /**
     * @var string
     */
    const _forward = '_forward';
}