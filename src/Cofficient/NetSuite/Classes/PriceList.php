<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * PriceList
 */
class PriceList
{
    /**
     * @access public
     * @var Price[]
     */
    public $price;
    static $paramtypesmap = array('price' => 'Price[]');
}