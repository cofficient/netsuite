<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * LandedCostDataList
 */
class LandedCostDataList
{
    /**
     * @access public
     * @var LandedCostData[]
     */
    public $landedCostData;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('landedCostData' => 'LandedCostData[]', 'replaceAll' => 'boolean');
}