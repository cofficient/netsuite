<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CurrencyRateList
 */
class CurrencyRateList
{
    /**
     * @access public
     * @var CurrencyRate[]
     */
    public $currencyRate;
    static $paramtypesmap = array('currencyRate' => 'CurrencyRate[]');
}