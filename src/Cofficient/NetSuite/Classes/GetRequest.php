<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetRequest
 */
class GetRequest
{
    /**
     * @access public
     * @var BaseRef
     */
    public $baseRef;
    static $paramtypesmap = array('baseRef' => 'BaseRef');
}