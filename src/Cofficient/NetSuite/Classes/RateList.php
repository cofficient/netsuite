<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * RateList
 */
class RateList
{
    /**
     * @access public
     * @var Rate[]
     */
    public $rate;
    static $paramtypesmap = array('rate' => 'Rate[]');
}