<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AsyncGetListRequest
 */
class AsyncGetListRequest
{
    /**
     * @access public
     * @var BaseRef[]
     */
    public $baseRef;
    static $paramtypesmap = array('baseRef' => 'BaseRef[]');
}