<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * FldFilterSelList
 */
class FldFilterSelList
{
    /**
     * @access public
     * @var RecordRef[]
     */
    public $fldFilterSel;
    static $paramtypesmap = array('fldFilterSel' => 'RecordRef[]');
}