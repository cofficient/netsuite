<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetCurrencyRateRequest
 */
class GetCurrencyRateRequest
{
    /**
     * @access public
     * @var CurrencyRateFilter
     */
    public $currencyRateFilter;
    static $paramtypesmap = array('currencyRateFilter' => 'CurrencyRateFilter');
}