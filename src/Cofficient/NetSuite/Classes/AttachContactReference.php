<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AttachContactReference
 */
class AttachContactReference extends AttachReference
{
    /**
     * @access public
     * @var RecordRef
     */
    public $contact;
    /**
     * @access public
     * @var RecordRef
     */
    public $contactRole;
    static $paramtypesmap = array('contact' => 'RecordRef', 'contactRole' => 'RecordRef');
}