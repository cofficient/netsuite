<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ExceededUsageLimitFault
 */
class ExceededUsageLimitFault extends NSSoapFault
{
    static $paramtypesmap = array();
}