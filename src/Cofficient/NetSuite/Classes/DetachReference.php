<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * DetachReference
 */
class DetachReference
{
    /**
     * @access public
     * @var BaseRef
     */
    public $detachFrom;
    static $paramtypesmap = array('detachFrom' => 'BaseRef');
}