<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ItemOptionsList
 */
class ItemOptionsList
{
    /**
     * @access public
     * @var RecordRef[]
     */
    public $itemOptions;
    static $paramtypesmap = array('itemOptions' => 'RecordRef[]');
}