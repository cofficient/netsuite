<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * BudgetExchangeRateList
 */
class BudgetExchangeRateList
{
    /**
     * @access public
     * @var BudgetExchangeRate[]
     */
    public $budgetExchangeRate;
    static $paramtypesmap = array('budgetExchangeRate' => 'BudgetExchangeRate[]');
}