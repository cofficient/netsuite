<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * BaseRefList
 */
class BaseRefList
{
    /**
     * @access public
     * @var BaseRef[]
     */
    public $baseRef;
    static $paramtypesmap = array('baseRef' => 'BaseRef[]');
}