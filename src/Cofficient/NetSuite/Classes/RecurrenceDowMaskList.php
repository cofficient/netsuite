<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * RecurrenceDowMaskList
 */
class RecurrenceDowMaskList
{
    /**
     * @access public
     * @var RecurrenceDow
     */
    public $recurrenceDowMask;
    static $paramtypesmap = array('recurrenceDowMask' => 'RecurrenceDow');
}