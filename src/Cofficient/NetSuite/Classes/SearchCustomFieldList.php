<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchCustomFieldList
 */
class SearchCustomFieldList
{
    /**
     * @access public
     * @var SearchCustomField[]
     */
    public $customField;
    static $paramtypesmap = array('customField' => 'SearchCustomField[]');
}