<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SearchBooleanCustomField
 */
class SearchBooleanCustomField extends SearchCustomField
{
    /**
     * @access public
     * @var boolean
     */
    public $searchValue;
    static $paramtypesmap = array('searchValue' => 'boolean');
}