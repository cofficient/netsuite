<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CustomRecordTypeManagers
 */
class CustomRecordTypeManagers
{
    /**
     * @access public
     * @var RecordRef
     */
    public $managerEmp;
    static $paramtypesmap = array('managerEmp' => 'RecordRef');
}