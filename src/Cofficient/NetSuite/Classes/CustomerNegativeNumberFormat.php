<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CustomerNegativeNumberFormat
 */
class CustomerNegativeNumberFormat
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _bracketSurrounded = '_bracketSurrounded';
    /**
     * @var string
     */
    const _minusSigned = '_minusSigned';
}