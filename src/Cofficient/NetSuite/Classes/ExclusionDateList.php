<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ExclusionDateList
 */
class ExclusionDateList
{
    /**
     * @access public
     * @var dateTime[]
     */
    public $exclusionDate;
    static $paramtypesmap = array('exclusionDate' => 'dateTime[]');
}