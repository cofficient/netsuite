<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CustomerMessageSearch
 */
class CustomerMessageSearch extends SearchRecord
{
    /**
     * @access public
     * @var CustomerMessageSearchBasic
     */
    public $basic;
    /**
     * @access public
     * @var EmployeeSearchBasic
     */
    public $userJoin;
    static $paramtypesmap = array('basic' => 'CustomerMessageSearchBasic', 'userJoin' => 'EmployeeSearchBasic');
}