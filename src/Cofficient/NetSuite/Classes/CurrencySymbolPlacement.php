<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CurrencySymbolPlacement
 */
class CurrencySymbolPlacement
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _afterNumber = '_afterNumber';
    /**
     * @var string
     */
    const _beforeNumber = '_beforeNumber';
}