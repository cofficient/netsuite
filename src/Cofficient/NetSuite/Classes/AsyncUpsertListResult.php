<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AsyncUpsertListResult
 */
class AsyncUpsertListResult extends AsyncResult
{
    /**
     * @access public
     * @var WriteResponseList
     */
    public $writeResponseList;
    static $paramtypesmap = array('writeResponseList' => 'WriteResponseList');
}