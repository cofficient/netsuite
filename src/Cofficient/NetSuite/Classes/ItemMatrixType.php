<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ItemMatrixType
 */
class ItemMatrixType
{
    static $paramtypesmap = array();
    /**
     * @var string
     */
    const _parent = '_parent';
    /**
     * @var string
     */
    const _child = '_child';
}