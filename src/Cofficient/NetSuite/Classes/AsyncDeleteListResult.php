<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AsyncDeleteListResult
 */
class AsyncDeleteListResult extends AsyncResult
{
    /**
     * @access public
     * @var WriteResponseList
     */
    public $writeResponseList;
    static $paramtypesmap = array('writeResponseList' => 'WriteResponseList');
}