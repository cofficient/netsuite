<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SsoLoginRequest
 */
class SsoLoginRequest
{
    /**
     * @access public
     * @var SsoPassport
     */
    public $ssoPassport;
    static $paramtypesmap = array('ssoPassport' => 'SsoPassport');
}