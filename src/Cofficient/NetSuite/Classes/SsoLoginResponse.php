<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SsoLoginResponse
 */
class SsoLoginResponse
{
    /**
     * @access public
     * @var SessionResponse
     */
    public $sessionResponse;
    static $paramtypesmap = array('sessionResponse' => 'SessionResponse');
}