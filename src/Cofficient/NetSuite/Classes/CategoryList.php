<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CategoryList
 */
class CategoryList
{
    /**
     * @access public
     * @var RecordRef[]
     */
    public $category;
    static $paramtypesmap = array('category' => 'RecordRef[]');
}