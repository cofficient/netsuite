<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * PromotionCodeItems
 */
class PromotionCodeItems
{
    /**
     * @access public
     * @var RecordRef
     */
    public $item;
    static $paramtypesmap = array('item' => 'RecordRef');
}