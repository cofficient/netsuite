<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * NullField
 */
class NullField
{
    /**
     * @access public
     * @var string[]
     */
    public $name;
    static $paramtypesmap = array('name' => 'string[]');
}