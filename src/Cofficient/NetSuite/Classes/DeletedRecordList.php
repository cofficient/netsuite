<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * DeletedRecordList
 */
class DeletedRecordList
{
    /**
     * @access public
     * @var DeletedRecord[]
     */
    public $deletedRecord;
    static $paramtypesmap = array('deletedRecord' => 'DeletedRecord[]');
}