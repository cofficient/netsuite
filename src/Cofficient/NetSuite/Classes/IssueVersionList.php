<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * IssueVersionList
 */
class IssueVersionList
{
    /**
     * @access public
     * @var IssueVersion[]
     */
    public $issueVersion;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('issueVersion' => 'IssueVersion[]', 'replaceAll' => 'boolean');
}