<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AsyncUpdateListRequest
 */
class AsyncUpdateListRequest
{
    /**
     * @access public
     * @var Record[]
     */
    public $record;
    static $paramtypesmap = array('record' => 'Record[]');
}