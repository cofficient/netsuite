<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * PeriodDemandPlan
 */
class PeriodDemandPlan
{
    /**
     * @access public
     * @var float
     */
    public $quantity;
    /**
     * @access public
     * @var DayOfTheWeek
     */
    public $dayOfTheWeek;
    static $paramtypesmap = array('quantity' => 'float', 'dayOfTheWeek' => 'DayOfTheWeek');
}