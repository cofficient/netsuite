<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * ChangeEmailResponse
 */
class ChangeEmailResponse
{
    /**
     * @access public
     * @var SessionResponse
     */
    public $sessionResponse;
    static $paramtypesmap = array('sessionResponse' => 'SessionResponse');
}