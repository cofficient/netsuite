<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * EstimatePartnersList
 */
class EstimatePartnersList
{
    /**
     * @access public
     * @var Partners[]
     */
    public $partners;
    /**
     * @access public
     * @var boolean
     */
    public $replaceAll;
    static $paramtypesmap = array('partners' => 'Partners[]', 'replaceAll' => 'boolean');
}