<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * BudgetSearch
 */
class BudgetSearch extends SearchRecord
{
    /**
     * @access public
     * @var BudgetSearchBasic
     */
    public $basic;
    static $paramtypesmap = array('basic' => 'BudgetSearchBasic');
}