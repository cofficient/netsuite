<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * AddResponse
 */
class AddResponse
{
    /**
     * @access public
     * @var WriteResponse
     */
    public $writeResponse;
    static $paramtypesmap = array('writeResponse' => 'WriteResponse');
}