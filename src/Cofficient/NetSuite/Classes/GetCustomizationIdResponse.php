<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetCustomizationIdResponse
 */
class GetCustomizationIdResponse
{
    /**
     * @access public
     * @var GetCustomizationIdResult
     */
    public $getCustomizationIdResult;
    static $paramtypesmap = array('getCustomizationIdResult' => 'GetCustomizationIdResult');
}