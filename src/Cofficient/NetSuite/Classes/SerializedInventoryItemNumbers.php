<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * SerializedInventoryItemNumbers
 */
class SerializedInventoryItemNumbers
{
    /**
     * @access public
     * @var RecordRef
     */
    public $serialNumber;
    static $paramtypesmap = array('serialNumber' => 'RecordRef');
}