<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * CustomRecordTypeChildren
 */
class CustomRecordTypeChildren
{
    /**
     * @access public
     * @var string
     */
    public $childDescr;
    /**
     * @access public
     * @var RecordRef
     */
    public $childTab;
    static $paramtypesmap = array('childDescr' => 'string', 'childTab' => 'RecordRef');
}