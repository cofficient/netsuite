<?php

namespace Cofficient\NetSuite\NetSuite;

/**
 * GetSavedSearchResponse
 */
class GetSavedSearchResponse
{
    /**
     * @access public
     * @var GetSavedSearchResult
     */
    public $getSavedSearchResult;
    static $paramtypesmap = array('getSavedSearchResult' => 'GetSavedSearchResult');
}