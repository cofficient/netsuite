<?php
/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 17/10/2015
 * Time: 09:41
 */

namespace Cofficient\NetSuite;

use Cofficient\NetSuite\NetSuite\NetSuiteService;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

class NetSuiteServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../../config/netsuite.php' => config_path('netsuite.php'),
        ]);

        $this->mergeConfigFrom(
            __DIR__ . '/../../config/netsuite.php', 'netsuite'
        );
    }

    public function register()
    {
        // TODO: Implement register() method.
        $this->app->singleton('netsuite', function()
        {
            // use global variable to stay compatible with NetSuite bad PHP Toolkit
            global $nsendpoint, $nshost, $nsemail,
                   $nspassword, $nsrole, $nsaccount;

            $nsendpoint = Config::get('netsuite.nsendpoint');
            $nshost     = Config::get('netsuite.nshost');
            $nsemail    = Config::get('netsuite.nsemail');
            $nspassword = Config::get('netsuite.nspassword');
            $nsrole     = Config::get('netsuite.nsrole');
            $nsaccount  = Config::get('netsuite.nsaccount');
            return new NetSuiteService();
        });
    }

    public function provides()
    {
        return array('netsuite');
    }
}