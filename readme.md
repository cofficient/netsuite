## NetSuite Laravel Wrapper

[ ![Codeship Status for cofficient/netsuite](https://codeship.com/projects/e350bb80-5ec0-0133-baf4-7294af506acb/status?branch=master)](https://codeship.com/projects/111510)

# Installation

First create a repository pointing to this package in your `composer.json`:

```php
"repositories": [
{
  "type": "vcs",
  "url": "https://bitbucket.org/cofficient/netsuite.git"
}
],
```

Add the package to your `composer.json`:

```php
"cofficient/netsuite": "dev-master"
```

Run `composer update` and add the provider to your app in `config/app.php`:

```php
'Cofficient\NetSuite\NetSuiteServiceProvider',
```

Create the facade shortcut by adding this to aliases:

```php
'NetSuite' => 'Cofficient\NetSuite\Facades\NetSuite',
```

Add the config file `excel.php` to your config folder by running:

```php
php artisan vendor:publish
```

Edit the config file `excel.php` to match your NetSuite settings.

The class is available through the IoC as `netsuite`.

```php
$netsuite = App:make('netsuite');
```