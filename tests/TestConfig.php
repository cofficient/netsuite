<?php



/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 24/10/2015
 * Time: 09:21
 */
namespace Cofficient\NetSuite\TestCase;

use Illuminate\Support\Facades\Config;
use Mockery as m;

class TestConfig extends TestCase
{
    public function tearDown() {
        parent::tearDown();
        m::close();
    }

    public function testCreatorConfig() {
        $this->assertEquals('2015_1', Config::get('netsuite.nsendpoint'));
    }
}