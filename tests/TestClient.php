<?php

/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 26/10/2015
 * Time: 10:28
 */

namespace Cofficient\NetSuite\TestCase;

use Cofficient\NetSuite\NetSuite\GetRequest;
use Cofficient\NetSuite\NetSuite\RecordRef;
use Cofficient\NetSuite\NetSuite\RecordType;
use Orchestra\Testbench\TestCase as TestBenchTestCase;
use Mockery as m;
use SoapVar;

class TestClient extends TestBenchTestCase
{
    public function testClient() {
        /** @var \Cofficient\NetSuite\NetSuite\NetSuiteService $netsuite */
        $netsuite = $this->app->make('netsuite');
        $request = new GetRequest();


        $recordRef = new RecordRef();
        $recordRef->internalId = '21';
        $recordRef->type = RecordType::customer;
        // Due to a bug in Soap client implementation of PHP
        // it is not possible to pass an object that inherent a abstract class in the WSDL
        // in our case RecordRef inherit BaseRef which is an abstract class
        // the work around is to wrap the object into a SoapVar object
        $request->baseRef= new SoapVar($recordRef, SOAP_ENC_OBJECT, 'RecordRef', 'urn:core_2015_1.platform.webservices.netsuite.com');

        $getResponse = $netsuite->get($request);
        $statusDetails = $getResponse->readResponse->status->statusDetail;
        $this->assertEquals('ERROR', $statusDetails[0]->type);
    }


    protected function getPackageProviders($app)
    {
        return ['Cofficient\NetSuite\NetSuiteServiceProvider'];
    }

    protected function getPackageAliases($app)
    {
        return [
            'NetSuite' => 'Cofficient\NetSuite\Facades\NetSuite'
        ];
    }

    public function tearDown() {
        parent::tearDown();
        m::close();
    }
}


