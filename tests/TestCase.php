<?php

/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 17/10/2015
 * Time: 08:28
 */

namespace Cofficient\NetSuite\TestCase;

use Orchestra\Testbench\TestCase as TestBenchTestCase;

class TestCase extends TestBenchTestCase
{

    public function testNetSuiteClass()
    {
        $netsuite = $this->app->make('netsuite');
        $this->assertInstanceOf('Cofficient\NetSuite\NetSuite\NetSuiteService', $netsuite);
    }

    protected function getPackageProviders($app)
    {
        return array('Cofficient\NetSuite\NetSuiteServiceProvider');

    }

    protected function getPackagePath()
    {
        return realpath(implode(DIRECTORY_SEPARATOR, array(__DIR__,'..','src','Cofficient','NetSuite')));
    }
}