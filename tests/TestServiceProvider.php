<?php
/**
 * Created by PhpStorm.
 * User: Benjamin
 * Date: 24/10/2015
 * Time: 09:28
 */
namespace Cofficient\NetSuite\TestCase;

use Cofficient\NetSuite\NetSuiteServiceProvider;
use Mockery as m;

class TestServiceProvider extends TestCase
{
    public function setUp() {
        parent::setUp();
    }

    public function tearDown() {
        parent::tearDown();
        m::close();
    }

    public function testProvider() {
        $app = m::mock('Illuminate\Foundation\Application');
        $provider = new NetSuiteServiceProvider($app);

        $this->assertCount(1, $provider->provides());
        $this->assertContains('netsuite', $provider->provides());
    }
}